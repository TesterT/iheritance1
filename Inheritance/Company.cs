﻿using System;
using System.Collections.Generic;
using System.Text;


namespace InheritanceTask
{
    public class Company 
    {
        private Employee[] employees;

        public Company(Employee[] employees)
        {
            this.employees = employees;
        }

    
        public void GiveEverybodyBonus(decimal companyBonus)
        {
            foreach (var employee in employees)
            {
                employee.SetBonus(companyBonus);
            }
        }

    public decimal TotalToPay()
        {
            decimal employeesSalary = 0;
            foreach (var employee in employees)
            {
                employeesSalary += employee.ToPay();
            }
            return employeesSalary;
        }

       
        public string NameMaxSalary()
        {
            int nameMaxIndex = 0;
            decimal maxSalary = employees[0].ToPay();
            for (int i = 0; i < employees.Length; i++)
            {
                if (employees[i].ToPay() > maxSalary)
                {
                    maxSalary = employees[i].ToPay();
                    nameMaxIndex = i;
                }
            }
            
            return employees[nameMaxIndex].Name;
        }

    }
}
